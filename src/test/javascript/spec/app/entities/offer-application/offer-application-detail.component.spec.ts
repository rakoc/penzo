import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InformacioniTestModule } from '../../../test.module';
import { OfferApplicationDetailComponent } from 'app/entities/offer-application/offer-application-detail.component';
import { OfferApplication } from 'app/shared/model/offer-application.model';

describe('Component Tests', () => {
  describe('OfferApplication Management Detail Component', () => {
    let comp: OfferApplicationDetailComponent;
    let fixture: ComponentFixture<OfferApplicationDetailComponent>;
    const route = ({ data: of({ offerApplication: new OfferApplication(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [InformacioniTestModule],
        declarations: [OfferApplicationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OfferApplicationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OfferApplicationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load offerApplication on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.offerApplication).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
