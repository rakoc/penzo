package com.info.sys.web.rest;

import com.info.sys.InformacioniApp;
import com.info.sys.domain.OfferApplication;
import com.info.sys.repository.OfferApplicationRepository;
import com.info.sys.service.OfferApplicationService;
import com.info.sys.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.info.sys.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.info.sys.domain.enumeration.Gender;
/**
 * Integration tests for the {@link OfferApplicationResource} REST controller.
 */
@SpringBootTest(classes = InformacioniApp.class)
public class OfferApplicationResourceIT {

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_AGE = "AAAAAAAAAA";
    private static final String UPDATED_AGE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private OfferApplicationRepository offerApplicationRepository;

    @Autowired
    private OfferApplicationService offerApplicationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOfferApplicationMockMvc;

    private OfferApplication offerApplication;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OfferApplicationResource offerApplicationResource = new OfferApplicationResource(offerApplicationService);
        this.restOfferApplicationMockMvc = MockMvcBuilders.standaloneSetup(offerApplicationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OfferApplication createEntity(EntityManager em) {
        OfferApplication offerApplication = new OfferApplication()
            .fullName(DEFAULT_FULL_NAME)
            .gender(DEFAULT_GENDER)
            .age(DEFAULT_AGE)
            .address(DEFAULT_ADDRESS)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .email(DEFAULT_EMAIL);
        return offerApplication;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OfferApplication createUpdatedEntity(EntityManager em) {
        OfferApplication offerApplication = new OfferApplication()
            .fullName(UPDATED_FULL_NAME)
            .gender(UPDATED_GENDER)
            .age(UPDATED_AGE)
            .address(UPDATED_ADDRESS)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL);
        return offerApplication;
    }

    @BeforeEach
    public void initTest() {
        offerApplication = createEntity(em);
    }

    @Test
    @Transactional
    public void createOfferApplication() throws Exception {
        int databaseSizeBeforeCreate = offerApplicationRepository.findAll().size();

        // Create the OfferApplication
        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isCreated());

        // Validate the OfferApplication in the database
        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeCreate + 1);
        OfferApplication testOfferApplication = offerApplicationList.get(offerApplicationList.size() - 1);
        assertThat(testOfferApplication.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testOfferApplication.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testOfferApplication.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testOfferApplication.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testOfferApplication.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testOfferApplication.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    public void createOfferApplicationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = offerApplicationRepository.findAll().size();

        // Create the OfferApplication with an existing ID
        offerApplication.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        // Validate the OfferApplication in the database
        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFullNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerApplicationRepository.findAll().size();
        // set the field null
        offerApplication.setFullName(null);

        // Create the OfferApplication, which fails.

        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerApplicationRepository.findAll().size();
        // set the field null
        offerApplication.setGender(null);

        // Create the OfferApplication, which fails.

        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerApplicationRepository.findAll().size();
        // set the field null
        offerApplication.setAge(null);

        // Create the OfferApplication, which fails.

        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerApplicationRepository.findAll().size();
        // set the field null
        offerApplication.setAddress(null);

        // Create the OfferApplication, which fails.

        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerApplicationRepository.findAll().size();
        // set the field null
        offerApplication.setPhoneNumber(null);

        // Create the OfferApplication, which fails.

        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerApplicationRepository.findAll().size();
        // set the field null
        offerApplication.setEmail(null);

        // Create the OfferApplication, which fails.

        restOfferApplicationMockMvc.perform(post("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOfferApplications() throws Exception {
        // Initialize the database
        offerApplicationRepository.saveAndFlush(offerApplication);

        // Get all the offerApplicationList
        restOfferApplicationMockMvc.perform(get("/api/offer-applications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(offerApplication.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }
    
    @Test
    @Transactional
    public void getOfferApplication() throws Exception {
        // Initialize the database
        offerApplicationRepository.saveAndFlush(offerApplication);

        // Get the offerApplication
        restOfferApplicationMockMvc.perform(get("/api/offer-applications/{id}", offerApplication.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(offerApplication.getId().intValue()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL));
    }

    @Test
    @Transactional
    public void getNonExistingOfferApplication() throws Exception {
        // Get the offerApplication
        restOfferApplicationMockMvc.perform(get("/api/offer-applications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOfferApplication() throws Exception {
        // Initialize the database
        offerApplicationService.save(offerApplication);

        int databaseSizeBeforeUpdate = offerApplicationRepository.findAll().size();

        // Update the offerApplication
        OfferApplication updatedOfferApplication = offerApplicationRepository.findById(offerApplication.getId()).get();
        // Disconnect from session so that the updates on updatedOfferApplication are not directly saved in db
        em.detach(updatedOfferApplication);
        updatedOfferApplication
            .fullName(UPDATED_FULL_NAME)
            .gender(UPDATED_GENDER)
            .age(UPDATED_AGE)
            .address(UPDATED_ADDRESS)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL);

        restOfferApplicationMockMvc.perform(put("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOfferApplication)))
            .andExpect(status().isOk());

        // Validate the OfferApplication in the database
        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeUpdate);
        OfferApplication testOfferApplication = offerApplicationList.get(offerApplicationList.size() - 1);
        assertThat(testOfferApplication.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testOfferApplication.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testOfferApplication.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testOfferApplication.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testOfferApplication.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testOfferApplication.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingOfferApplication() throws Exception {
        int databaseSizeBeforeUpdate = offerApplicationRepository.findAll().size();

        // Create the OfferApplication

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOfferApplicationMockMvc.perform(put("/api/offer-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(offerApplication)))
            .andExpect(status().isBadRequest());

        // Validate the OfferApplication in the database
        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOfferApplication() throws Exception {
        // Initialize the database
        offerApplicationService.save(offerApplication);

        int databaseSizeBeforeDelete = offerApplicationRepository.findAll().size();

        // Delete the offerApplication
        restOfferApplicationMockMvc.perform(delete("/api/offer-applications/{id}", offerApplication.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OfferApplication> offerApplicationList = offerApplicationRepository.findAll();
        assertThat(offerApplicationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
