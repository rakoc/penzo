package com.info.sys.repository;

import com.info.sys.domain.OfferApplication;
import com.info.sys.service.dto.OfferApplicationDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OfferApplication entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OfferApplicationRepository extends JpaRepository<OfferApplication, Long> {

    @Query("select new com.info.sys.service.dto.OfferApplicationDTO(oa.fullName, oa.address, oa.offer) from OfferApplication oa inner join Offer o on oa.offer = o where o.id = ?1")
    OfferApplicationDTO getAllOfferWhereIdIs(Long id);
}
