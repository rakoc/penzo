package com.info.sys.service.dto;

import com.info.sys.domain.Offer;

public class OfferApplicationDTO {

    private String name;
    private String address;
    private Offer offer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setCity(Offer offer) {
        this.offer = offer;
    }

    public OfferApplicationDTO(String name, String address, Offer offer) {
        this.name = name;
        this.address = address;
        this.offer = offer;
    }
}
