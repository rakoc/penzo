/**
 * View Models used by Spring MVC REST controllers.
 */
package com.info.sys.web.rest.vm;
