import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { InformacioniSharedModule } from 'app/shared/shared.module';
import { InformacioniCoreModule } from 'app/core/core.module';
import { InformacioniAppRoutingModule } from './app-routing.module';
import { InformacioniHomeModule } from './home/home.module';
import { InformacioniEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    InformacioniSharedModule,
    InformacioniCoreModule,
    InformacioniHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    InformacioniEntityModule,
    InformacioniAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class InformacioniAppModule {}
