import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'city',
        loadChildren: () => import('./city/city.module').then(m => m.InformacioniCityModule)
      },
      {
        path: 'offer',
        loadChildren: () => import('./offer/offer.module').then(m => m.InformacioniOfferModule)
      },
      {
        path: 'offer-application',
        loadChildren: () => import('./offer-application/offer-application.module').then(m => m.InformacioniOfferApplicationModule)
      },
      {
        path: 'question',
        loadChildren: () => import('./question/question.module').then(m => m.InformacioniQuestionModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class InformacioniEntityModule {}
