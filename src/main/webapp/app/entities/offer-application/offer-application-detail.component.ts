import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOfferApplication } from 'app/shared/model/offer-application.model';

@Component({
  selector: 'jhi-offer-application-detail',
  templateUrl: './offer-application-detail.component.html'
})
export class OfferApplicationDetailComponent implements OnInit {
  offerApplication: IOfferApplication | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ offerApplication }) => {
      this.offerApplication = offerApplication;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
