import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOfferApplication, OfferApplication } from 'app/shared/model/offer-application.model';
import { OfferApplicationService } from './offer-application.service';
import { IOffer } from 'app/shared/model/offer.model';
import { OfferService } from 'app/entities/offer/offer.service';

@Component({
  selector: 'jhi-offer-application-update',
  templateUrl: './offer-application-update.component.html'
})
export class OfferApplicationUpdateComponent implements OnInit {
  isSaving = false;

  offers: IOffer[] = [];

  editForm = this.fb.group({
    id: [],
    fullName: [null, [Validators.required]],
    gender: [null, [Validators.required]],
    age: [null, [Validators.required]],
    address: [null, [Validators.required]],
    phoneNumber: [null, [Validators.required]],
    email: [null, [Validators.required]],
    offer: []
  });

  constructor(
    protected offerApplicationService: OfferApplicationService,
    protected offerService: OfferService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ offerApplication }) => {
      this.updateForm(offerApplication);

      this.offerService
        .query()
        .pipe(
          map((res: HttpResponse<IOffer[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IOffer[]) => (this.offers = resBody));
    });
  }

  updateForm(offerApplication: IOfferApplication): void {
    this.editForm.patchValue({
      id: offerApplication.id,
      fullName: offerApplication.fullName,
      gender: offerApplication.gender,
      age: offerApplication.age,
      address: offerApplication.address,
      phoneNumber: offerApplication.phoneNumber,
      email: offerApplication.email,
      offer: offerApplication.offer
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const offerApplication = this.createFromForm();
    if (offerApplication.id !== undefined) {
      this.subscribeToSaveResponse(this.offerApplicationService.update(offerApplication));
    } else {
      this.subscribeToSaveResponse(this.offerApplicationService.create(offerApplication));
    }
  }

  private createFromForm(): IOfferApplication {
    return {
      ...new OfferApplication(),
      id: this.editForm.get(['id'])!.value,
      fullName: this.editForm.get(['fullName'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      age: this.editForm.get(['age'])!.value,
      address: this.editForm.get(['address'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      email: this.editForm.get(['email'])!.value,
      offer: this.editForm.get(['offer'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOfferApplication>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOffer): any {
    return item.id;
  }
}
