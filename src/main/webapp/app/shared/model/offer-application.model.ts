import { IOffer } from 'app/shared/model/offer.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface IOfferApplication {
  id?: number;
  fullName?: string;
  gender?: Gender;
  age?: string;
  address?: string;
  phoneNumber?: string;
  email?: string;
  offer?: IOffer;
}

export class OfferApplication implements IOfferApplication {
  constructor(
    public id?: number,
    public fullName?: string,
    public gender?: Gender,
    public age?: string,
    public address?: string,
    public phoneNumber?: string,
    public email?: string,
    public offer?: IOffer
  ) {}
}
