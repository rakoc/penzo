import { ICity } from 'app/shared/model/city.model';

export interface IOffer {
  id?: number;
  name?: string;
  price?: string;
  time?: string;
  capacity?: string;
  city?: ICity;
}

export class Offer implements IOffer {
  constructor(
    public id?: number,
    public name?: string,
    public price?: string,
    public time?: string,
    public capacity?: string,
    public city?: ICity
  ) {}
}
