export interface ICity {
  id?: number;
  name?: string;
  description?: string;
  imageContentType?: string;
  image?: any;
}

export class City implements ICity {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public imageContentType?: string,
    public image?: any
  ) {}
}
