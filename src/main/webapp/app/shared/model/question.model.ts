export interface IQuestion {
  id?: number;
  fullName?: string;
  email?: string;
  question?: string;
}

export class Question implements IQuestion {
  constructor(public id?: number, public fullName?: string, public email?: string, public question?: string) {}
}
